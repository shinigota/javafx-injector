/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;

import com.shinigota.jfxinjector.buttons.ButtonCellDelete;
import com.shinigota.jfxinjector.buttons.ButtonCellModify;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.commons.beanutils.PropertyUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by bbarbe on 31/03/2016.
 */
public class FXITableViewer<O> {
    private static final double PADDING = 10;
    private AnchorPane mainPane;
    private TableView tableView;
    private FXITableListener modifyCallback;
    private FXITableListener deletCallback;

    public <T extends O> FXITableViewer(final Collection<O> objects, final Class<? extends O> type, final FXITableListener modifyCallback, final FXITableListener deleteCallback) {
        this.tableView = createTableView(objects, type);
        this.modifyCallback = modifyCallback;
        this.deletCallback = deleteCallback;
        mainPane = new AnchorPane();
        AnchorPane.setBottomAnchor(tableView, PADDING);
        AnchorPane.setLeftAnchor(tableView, PADDING);
        AnchorPane.setRightAnchor(tableView, PADDING);
        AnchorPane.setTopAnchor(tableView, PADDING);
        mainPane.getChildren().add(tableView);
    }

    private <T extends O> TableView createTableView(final Collection<O> objects, final Class<? extends O> type) {
        ArrayList<TableColumn> columns = new ArrayList<TableColumn>();
        System.out.println(type.getName() + " " + type.getDeclaredFields().length);
        for(Field field : type.getDeclaredFields()) {
            System.out.println(field.getName());
            if(field.isAnnotationPresent(FXMLField.class)) {
                TableColumn column = new TableColumn(FXIObject.fieldToString(field));
                column.setCellFactory(TextFieldTableCell.forTableColumn());
                column.setCellValueFactory(data -> {
                    try {
                        return new ReadOnlyStringWrapper(PropertyUtils.getProperty(((TableColumn.CellDataFeatures)data).getValue(), field.getName()).toString());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    return null;
                });
                columns.add(column);

            }
        }

        TableView tableView = new TableView();

        for(TableColumn column : columns) {
            column.prefWidthProperty().bind(tableView.widthProperty().subtract(200).divide(columns.size()));
        }

        TableColumn modifyColumn = new TableColumn("Modify");
        modifyColumn.setCellValueFactory(p -> new SimpleBooleanProperty(((TableColumn.CellDataFeatures)p).getValue() != null));
        modifyColumn.setCellFactory(p -> new ButtonCellModify(tableView, modifyCallback));
        modifyColumn.setPrefWidth(100);
        columns.add(modifyColumn);

        TableColumn deleteColumn = new TableColumn("Delete");
        deleteColumn.setCellValueFactory(p -> new SimpleBooleanProperty(((TableColumn.CellDataFeatures)p).getValue() != null));
        deleteColumn.setCellFactory(p -> new ButtonCellDelete(tableView, deletCallback));
        deleteColumn.setPrefWidth(100);
        columns.add(deleteColumn);

        tableView.getColumns().addAll(columns);
        tableView.getItems().addAll(objects);

        return tableView;
    }

    public AnchorPane getMainPane() {
        return mainPane;
    }

}
