/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;

import javafx.geometry.Insets;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The FXIObject type, corresponding to an object and controls to display or modify it.
 *
 * @param <T> the type of the object
 */
public abstract class FXIObject<T> {
    /**
     * The FXIObject's Stage the view will be displayed on.
     */
    private final Stage stage;
    /**
     * The FXIObject's Main pane containing the controls.
     */
    protected GridPane mainPane;
    /**
     * The FXIObject's Controls.
     */
    protected Map<Field, Control> controls;
    /**
     * The FXIObject's Object.
     */
    protected T object;

    /**
     * Instantiates a new FXIObject.
     *
     * @param type  the type of object being in use
     * @param stage the stage
     * @throws IllegalAccessException the illegal access exception
     * @throws InstantiationException the instantiation exception
     */
    public FXIObject(final Class<T> type, final Stage stage) throws IllegalAccessException, InstantiationException {
        this.stage = stage;
        setCurrentObject(type.newInstance());
//        stage.setResizable(false);

        mainPane = new GridPane();
        setGridConstraints();

        controls = new HashMap<Field, Control>();

    }

    protected void setCurrentObject(T object) {
        this.object = object;
    }

    /**
     * Converts a field's name into a normally formatted String
     *
     * @param field the field being converted
     * @return the string
     */
    public static String fieldToString(final Field field) {
        String parsedName = field.getName().replaceAll("(.)([A-Z]|[0-9])", "$1 $2");
        return parsedName.substring(0, 1).toUpperCase() + parsedName.substring(1, parsedName.length()).toLowerCase();
    }

    /**
     * Create a label from a field.
     *
     * @param field the field
     * @return the label
     */
    protected Label createLabel(final Field field) {
        final FXMLField annotation = field.getAnnotation(FXMLField.class);
        String labelName = "";
        if(!annotation.label().equals("")) {
            labelName = annotation.label().equals("") ? field.getName() : annotation.label();
        } else {
            labelName = fieldToString(field);
        }

        return new Label(labelName + " :");
    }

    /**
     * Sets grid constraints.
     */
    private void setGridConstraints() {
        mainPane.setVgap(10);
        mainPane.setHgap(10);
        mainPane.setPadding(new Insets(10));
        mainPane.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        ColumnConstraints constraintsLabelField = new ColumnConstraints();
        constraintsLabelField.setMaxWidth(Region.USE_COMPUTED_SIZE);
        ColumnConstraints constraintsColField = new ColumnConstraints();
        constraintsColField.setMaxWidth(Double.MAX_VALUE);
        constraintsColField.setHgrow(Priority.ALWAYS);
        mainPane.getColumnConstraints().addAll(constraintsLabelField, constraintsColField);
    }

    /**
     * Gets annotated fields.
     *
     * @param type the type
     * @return the annotated fields
     */
    protected List<Field> getAnnotatedFields(final Class<T> type) {
        List<Field> annotatedFields = new ArrayList<Field>();
        Field[] allFields = type.getDeclaredFields();

        for(Field field : allFields) {
            System.out.println(field.getName() + " " + field.isAnnotationPresent(FXMLField.class));
            if(field.isAnnotationPresent(FXMLField.class)) {
                annotatedFields.add(field);
            }
        }
        return annotatedFields;
    }

    /**
     * Gets main pane.
     *
     * @return the main pane
     */
    public final Pane getMainPane() {
        return mainPane;
    }

    /**
     * Show and wait.
     */
    public void showAndWait() {
        stage.showAndWait();
    }
}
