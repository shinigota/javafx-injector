/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.ZoneId;
import java.util.*;

/**
 * The type FXIObjectCreator.
 * View that can create an object from type T by generating a control for each annotated T fields
 * @param <T> the type parameter
 */
public class FXIObjectCreator<T> extends FXIObject {
    /**
     * The FXIObjectCreator's validation button.
     * Validates the form to instanciate the object
     */
    private Button validate;

    /**
     * The FXIObjectCreator's cancel button.
     * Cancels the form edition
     */
    private Button cancel;

    /**
     * The FXIObjectCreator's finished state.
     * Used to know if the object has been created and if the view is closed.
     */
    private boolean finished;


    /**
     * Instantiates a new FXIObjectCreator view, with a control for each annotated class's field.
     *
     * @param type  the type the view will be based on
     * @param stage the stage used to display the view
     * @throws IllegalAccessException the illegal access exception
     * @throws InstantiationException the instantiation exception
     */
    public FXIObjectCreator(final Class<T> type, final Stage stage) throws IllegalAccessException, InstantiationException {
        super(type, stage);


        List<Field> annotatedFields = getAnnotatedFields(type);

        int fieldNumber = createGridControlRows(annotatedFields);

        cancel = new Button("Cancel");
        GridPane.setValignment(cancel, VPos.BOTTOM);
        cancel.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(final ActionEvent event) {
                stage.close();
            }
        });

        validate = new Button("Validate");
        GridPane.setValignment(validate, VPos.BOTTOM);
        validate.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(final ActionEvent event) {
                finished = true;
                Set<Map.Entry<Field, Control>> entrySet = controls.entrySet();

                for(Map.Entry<Field, Control> entry : entrySet) {
                    Object valueToSet = getControlValue(entry.getValue());
                    if(!entry.getKey().getAnnotation(FXMLField.class).nullable() && valueToSet == null) {
                        finished = false;
                        displayErrorTooltip(entry, stage);
                    } else {
                        setObjectValue(entry, valueToSet);
                    }
                }

                if(finished)
                    stage.close();
            }
        });

        mainPane.add(validate, 0, fieldNumber);
        mainPane.add(cancel, 1, fieldNumber);

        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setMaxHeight(Double.MAX_VALUE);
        rowConstraints.setVgrow(Priority.ALWAYS);
        mainPane.getRowConstraints().add(rowConstraints);

        finished = false;
    }

    /**
     * Creates grid rows containing the controls.
     *
     * @param annotatedFields the annotated fields that will be added to the grid
     * @return the number of rows that has been created
     */
    private int createGridControlRows(final List<Field> annotatedFields) {
        int numberOfRows = 0;
        for(Field field : annotatedFields) {
            Control control = createControlFromField(field);

            control.setMaxWidth(Double.MAX_VALUE);
            Label label = createLabel(field);

            controls.put(field, control);

            RowConstraints rowConstraints = new RowConstraints();
            mainPane.getRowConstraints().add(rowConstraints);
            mainPane.add(label, 0, numberOfRows);
            mainPane.add(control, 1, numberOfRows);

            numberOfRows ++;
        }
        return numberOfRows;
    }

    /**
     * Sets a value to the object being created.
     *
     * @param entry      the entry, containing a field and its control
     * @param valueToSet the value to set to the object
     */
    private void setObjectValue(final Map.Entry<Field, Control> entry, final Object valueToSet) {
        try {
            BeanUtils.setProperty(object, entry.getKey().getName(), valueToSet);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets a control's value.
     *
     * @param control the control the value is being retrieved on
     * @return the control value
     */
    private Object getControlValue(final Control control) {
        Object valueToSet = null;
        if(control instanceof TextField) {
            valueToSet = ((TextField)control).getText();
            if(valueToSet.equals(""))
                valueToSet = null;
        } else if (control instanceof ComboBox) {
            valueToSet= ((ComboBox)control).getValue();
        } else if (control instanceof DatePicker) {
            valueToSet =  Date.from(((DatePicker)control).getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
        } else if (control instanceof CheckBox) {
            valueToSet =  ((CheckBox)control).isSelected();
        }
        return valueToSet;
    }

    /**
     * Display an error tooltip for a given type.
     * The error displays the field description.
     *
     * @param entry the entry, containing a field and its control
     * @param stage the stage the tolltip is being shown on
     */
    private void displayErrorTooltip(final Map.Entry<Field, Control> entry, final Stage stage) {
        final Control control = entry.getValue();
        MenuItem item = new MenuItem(entry.getKey().getAnnotation(FXMLField.class).description());
        item.setStyle("-fx-text-fill: rgb(255,255,255); ");
        ContextMenu cm = new ContextMenu(item);
        cm.setAutoHide(true);
        cm.setHeight(control.getHeight());
        cm.setStyle("-fx-background-color: rgba(0, 0, 0, 0.50); -fx-border-radius: 2px");
        cm.show(stage, control.getScene().getX() + control.getScene().getWindow().getX() + control.getLayoutX(), control.getScene().getY() + control.getScene().getWindow().getY() + control.getLayoutY());
    }

    /**
     * Create a control from a field.
     *
     * @param field the field used to create the control
     * @return the control corresponding to the field's type
     */
    private Control createControlFromField(final Field field) {
        final ControlType controlType = field.getAnnotation(FXMLField.class).control();
        Control control;
        if(controlType == ControlType.FROM_TYPE) {
            if(field.getType().toString().equals("boolean")) {
                control = createCheckBox(field);
            } else if(field.getType().isPrimitive() || field.getType().toString().equals(String.class.toString())) {
                control = createTextField(field);
            } else if (field.getType().toString().equals(Date.class.toString())){
                control = createDatePicker();
            } else {
                control = createComboBox(field);
            }
        } else {
            control = createComboBox(field);
        }
        return control;
    }

    /**
     * Create date picker control.
     *
     * @return the control
     */
    private Control createDatePicker() {
        return new DatePicker(new GregorianCalendar().getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
    }

    /**
     * Create text field text field.
     *
     * @param field the field
     * @return the text field
     */
    private TextField createTextField(final Field field) {
        FXMLField annotation = field.getAnnotation(FXMLField.class);
        String regex = getFieldRegex(field);
        TextField tf = new TextField();
        setTextFieldFilter(tf, regex);
        tf.setTooltip(new Tooltip(annotation.description()));

        return tf;
    }

    /**
     * Create check box check box.
     *
     * @param field the field
     * @return the check box
     */
    private CheckBox createCheckBox(final Field field) {
        FXMLField annotation = field.getAnnotation(FXMLField.class);
        CheckBox cb = new CheckBox("Is " + FXIObject.fieldToString(field).toLowerCase());
        cb.setTooltip(new Tooltip(annotation.description()));

        return cb;
    }

    /**
     * Create combo box combo box.
     *
     * @param field the field
     * @return the combo box
     */
    private ComboBox<T> createComboBox(final Field field) {
        FXMLField annotation = field.getAnnotation(FXMLField.class);
        String regex = getFieldRegex(field);
        ComboBox<T> comboBox = new ComboBox<T>();
        setTextFieldFilter(comboBox.getEditor(), regex);
        comboBox.setTooltip(new Tooltip(annotation.description()));

        return comboBox;
    }

    /**
     * Bind data.
     *
     * @param fieldName the field name
     * @param elements  the elements
     */
    public void bindData(String fieldName, List<T> elements) {
        Set<Map.Entry<Field, Control>> entrySet = controls.entrySet();
        for(Map.Entry<Field, Control> entry : entrySet) {
            if(entry.getKey().getName().equals(fieldName)) {
                if(entry.getValue() instanceof ComboBox)
                ((ComboBox)entry.getValue()).getItems().addAll(elements);
            }
        }

    }

    /**
     * Gets field regex.
     *
     * @param field the field
     * @return the field regex
     */
    private String getFieldRegex(final Field field) {
        FXMLField annotation = field.getAnnotation(FXMLField.class);
        String regex;
        if(annotation.regex().equals(FXIRegex.FROM_TYPE)) {
            String type = field.getType().toString();
            if(type.equals(String.class.toString())) {
                regex = FXIRegex.ANY;
            } else if(type.equals("int") || type.equals("byte") || type.equals("short") || type.equals("long")) {
                regex = FXIRegex.INTEGER;
            } else if(type.equals("float") || type.equals("double")) {
                regex = FXIRegex.DECIMAL;
            } else if(type.equals("char")) {
                regex = FXIRegex.CHAR;
            } else if(type.equals("boolean")) {
                regex = FXIRegex.BOOLEAN;
            } else {
                regex = FXIRegex.ANY;
            }
        } else {
            regex = annotation.regex();
        }
        return regex;
    }

    /**
     * Sets text field filter.
     *
     * @param textField the text field
     * @param regex     the regex
     */
    private void setTextFieldFilter(final TextField textField, final String regex) {
        textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> observable, final String oldValue, final String newValue) {
                if(!newValue.matches(regex)) {
                    textField.setText(oldValue);
                }
            }
        });

    }

    /**
     * Is finished boolean.
     *
     * @return the boolean
     */
    public final boolean isFinished() {
        return finished;
    }

    /**
     * Gets object.
     *
     * @return the object
     */
    public final T getObject() {
        return (T) object;
    }
}
