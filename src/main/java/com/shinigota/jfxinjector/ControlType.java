/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;


/**
 * The enum Control type.
 */
public enum ControlType {
    /**
     * Textfield control type.
     */
    TEXTFIELD,
    /**
     * Combobox control type.
     */
    COMBOBOX,
    /**
     * Choicebox control type.
     */
    CHOICEBOX,
    /**
     * Checkbox control type.
     */
    CHECKBOX,
    /**
     * Datepicker control type.
     */
    DATEPICKER,
    /**
     * From type control type.
     */
    FROM_TYPE
}
