/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector.buttons;

import com.shinigota.jfxinjector.FXITableListener;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;


/**
 * The type Button cell.
 * Button displayed on a TableView's cell to interact with the row's object
 */
public abstract class ButtonCell extends TableCell {
    /**
     * The ButtonCell's button.
     */
    private final Button buttonCell;

    /**
     * Instantiates a new Button cell.
     *
     * @param buttonText the button's text displayed
     * @param tableView  the parent table view
     * @param callback   the callback called when the button is pressed
     */
    public ButtonCell(final String buttonText, final TableView tableView, final FXITableListener callback){
        buttonCell = new Button(buttonText);
        setAlignment(Pos.CENTER);
        buttonCell.setOnAction(t -> {
            int selectedRowIndex = getTableRow().getIndex();
            Object object = tableView.getItems().get(selectedRowIndex);

            action(tableView, object);

            ((TableColumn)tableView.getColumns().get(0)).setVisible(false);
            ((TableColumn)tableView.getColumns().get(0)).setVisible(true);

            if(callback != null) {
                callback.action(object);
            }

        });


    }

    /**
     * Updates the parent cell
     *
     * @param item  the item being displayed
     * @param empty the row being empty or nor
     */
    @Override
    protected void updateItem(final Object item, final boolean empty) {
        super.updateItem(item, empty);
        if(!empty){
            setGraphic(buttonCell);
        } else {
            setGraphic(null);
        }
    }

    /**
     * The button's action
     *
     * @param tableView the tableView the button is acting on
     * @param object    the object the button is acting on
     */
    protected abstract void action(final TableView tableView, final Object object);
}
