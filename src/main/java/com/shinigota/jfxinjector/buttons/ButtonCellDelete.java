/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector.buttons;

import com.shinigota.jfxinjector.FXITableListener;
import javafx.scene.control.TableView;

/**
 * The type ButtonCellDelete.
 * Button deleting the object's on its row
 */
public class ButtonCellDelete extends ButtonCell {

    /**
     * Instantiates a new ButtonCellDelete.
     *
     * @param tableView      the table view
     * @param deleteListener the callback called when the button is pressed
     */
    public ButtonCellDelete(final TableView tableView, final FXITableListener deleteListener){
        super("Delete", tableView, deleteListener);
    }

    @Override
    protected void action(final TableView tableView, final Object object) {
        tableView.getItems().remove(object);
    }

}