/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector.buttons;

import com.shinigota.jfxinjector.FXIObjectModifier;
import com.shinigota.jfxinjector.FXITableListener;
import com.shinigota.jfxinjector.FXIViewBuilder;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 * The type ButtonCellModify.
 * Button displaying a view used to modify the object on the button's row
 */
public class ButtonCellModify extends ButtonCell {

    /**
     * Instantiates a new ButtonCellModify.
     *
     * @param tableView      the table view
     * @param modifyListener the callback called when the button is pressed
     */
    public ButtonCellModify(final TableView tableView, final FXITableListener modifyListener){
        super("Modify", tableView, modifyListener);
    }

    @Override
    protected void action(final TableView tableView, final Object object) {
        FXIObjectModifier objectModifier = FXIViewBuilder.buildModifier(object, (Stage) getTableView().getParent().getScene().getWindow());
        FXIViewBuilder.consumeCreator(objectModifier);
    }
}