/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;

import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;


/**
 * The type FXIViewBuilder.
 * Static class used to build the differents views
 */
public class FXIViewBuilder {
    /**
     * Build a FXIObjectCreator view that can create a T object.
     *
     * @param <T>         the type of object being created
     * @param type        the type of object being created
     * @param parentStage the parent stage
     * @return the FXIObjectCreator
     */
    public static <T> FXIObjectCreator buildCreator(final Class<T> type, final Stage parentStage) {
        Stage stage = new Stage();
        stage.initOwner(parentStage);
        FXIObjectCreator view = null;
        try {
            view = new FXIObjectCreator(type, stage);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(view.getMainPane()));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        return view;
    }

    /**
     * Build a FXIObjectModifier view that can modify a T object.
     *
     * @param <T>         the type of object being modified
     * @param object      the object being modified
     * @param parentStage the parent stage
     * @return the FXIObjectModifier
     */
    public static <T> FXIObjectModifier buildModifier(final T object, final Stage parentStage) {
        Stage stage = new Stage();
        stage.initOwner(parentStage);
        FXIObjectModifier view = null;
        try {
            view = new FXIObjectModifier(object, stage);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(view.getMainPane()));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return view;
    }

    /**
     * Consume a FXIObjectCreator (or FXIObjectModifier) by showing it and retrieving its result.
     *
     * @param <T>  the type type of object being created (or modified)
     * @param view the FXIObjectCreator view
     * @return the newly created or modified object
     */
    public static <T> T consumeCreator(final FXIObjectCreator view) {
        view.showAndWait();

        T finalObject = null;

        if(view.isFinished())
            finalObject = (T) view.getObject();

        return finalObject;
    }

    /**
     * Build a FXIObjectViewer to display an object.
     *
     * @param obj         the obj that needs to be displayed
     * @param parentStage the parent stage
     */
    public static void buildViewer(final Object obj, final Stage parentStage) {
        Stage stage = new Stage();
        stage.initOwner(parentStage);
        FXIObjectViewer view = null;
        try {
            view = new FXIObjectViewer(obj, stage);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(new Scene(view.getMainPane()));
        stage.showAndWait();
    }

    public static <O> FXITableViewer buildTableViewer(final ArrayList<O> objects, final Class<? extends O> type, final Stage parentStage) {
        Stage stage = new Stage();
        stage.initOwner(parentStage);
        FXITableViewer view = null;
        view = new FXITableViewer<O>(objects, type, null, null);

        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(new Scene(view.getMainPane()));
        stage.showAndWait();

        return view;
    }

    public static <O> FXITableViewer buildTableViewer(final Collection<O> objects, final Class<? extends O> type, final Stage parentStage, final FXITableListener modifyCallback, final FXITableListener deleteCallback) {
        Stage stage = new Stage();
        stage.initOwner(parentStage);
        FXITableViewer view = null;
        view = new FXITableViewer<O>(objects, type, modifyCallback, deleteCallback);

        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(new Scene(view.getMainPane()));
        stage.showAndWait();

        return view;
    }
}
