/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface FXMLField {
    /**
     * Label string.
     *
     * @return the string
     */
    String label() default "";

    /**
     * Control control type.
     *
     * @return the control type
     */
    ControlType control() default ControlType.FROM_TYPE;

    /**
     * Regex string.
     *
     * @return the string
     */
    String regex() default FXIRegex.FROM_TYPE;

    /**
     * Nullable boolean.
     *
     * @return the boolean
     */
    boolean nullable() default false;

    /**
     * Description string.
     *
     * @return the string
     */
    String description() default "Mandatory field.";
}
