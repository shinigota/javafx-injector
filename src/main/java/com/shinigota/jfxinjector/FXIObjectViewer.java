/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import org.apache.commons.beanutils.PropertyUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * The type FXIObjectViewer.
 * View that shows annotated fields from an object
 * @param <O> the type parameter
 */
public class FXIObjectViewer<O> extends FXIObject {
    /**
     * The FXIObjectViewer's exit button.
     */
    private final Button exit;

    /**
     * Instantiates a new FXIObjectViewer.
     *
     * @param object the object to view
     * @param stage  the stage
     * @throws IllegalAccessException    the illegal access exception
     * @throws InstantiationException    the instantiation exception
     * @throws InvocationTargetException the invocation target exception
     * @throws NoSuchMethodException     the no such method exception
     */
    public <T extends O> FXIObjectViewer (final T object, final Stage stage) throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        super(object.getClass(), stage);
        this.object = object;
        List<Field> annotatedFields = getAnnotatedFields(object.getClass());

        int fieldNumber = 0;
        for(Field field : annotatedFields) {
            Label valueLabel = createValueLabel(field);
            valueLabel.setMaxWidth(Double.MAX_VALUE);
            Label label = createLabel(field);

            controls.put(field, valueLabel);

            RowConstraints rowConstraints = new RowConstraints();
            mainPane.getRowConstraints().add(rowConstraints);
            mainPane.add(label, 0, fieldNumber);
            mainPane.add(valueLabel, 1, fieldNumber);

            fieldNumber ++;
        }

        exit = new Button("Exit");
        GridPane.setValignment(exit, VPos.BOTTOM);
        exit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(final ActionEvent event) {
                stage.close();
            }
        });

        mainPane.add(exit, 0, fieldNumber);

        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setMaxHeight(Double.MAX_VALUE);
        rowConstraints.setVgrow(Priority.ALWAYS);
        mainPane.getRowConstraints().add(rowConstraints);
    }

    /**
     * Create a value label showing a field's value.
     *
     * @param field the field that need to be displayed
     * @return the label that can be displayed containing the field's value
     * @throws IllegalAccessException    the illegal access exception
     * @throws InvocationTargetException the invocation target exception
     * @throws NoSuchMethodException     the no such method exception
     */
    private Label createValueLabel(final Field field) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Label label = new Label(PropertyUtils.getProperty(object, field.getName()).toString());
        label.setAlignment(Pos.CENTER_RIGHT);
        return label;
    }
}
