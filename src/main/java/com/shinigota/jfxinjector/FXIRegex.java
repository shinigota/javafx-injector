/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;


/**
 * Class containing the jfx-injector regex used in textfields
 */
public class FXIRegex {
    /**
     * The FXIRegex's constant DECIMAL.
     * Regex that matches decimals (double, float)
     */
    public final static String DECIMAL = "(\\d+(\\.\\d*)?)?";
    /**
     * The FXIRegex's constant INTEGER.
     * Regex that matches decimals integers (int, long)
     */
    public final static String INTEGER = "\\d*";
    /**
     * The FXIRegex's constant CHAR.
     * Regex that matches chars
     */
    public final static String CHAR = ".?";
    /**
     * The FXIRegex's constant BOOLEAN.
     * Regex that matches booleans (true, false, 1, 0, non case sensitive)
     */
    public final static String BOOLEAN = "(?i)(true|false|1|0)?";
    /**
     * The FXIRegex's constant ANY.
     * Regex that matches any string
     */
    public final static String ANY = ".*";
    /**
     * The FXIRegex's constant FROM_TYPE.
     * Regex that matches the object's parameter's type
     */
    public final static String FROM_TYPE = "";
}
