/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;


import javafx.scene.control.*;
import javafx.stage.Stage;
import org.apache.commons.beanutils.PropertyUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * The type FXIObjectModifier.
 * View that can be used to modify an object
 *
 * @param <T> the type of the object
 */
public class FXIObjectModifier<T> extends FXIObjectCreator {
    /**
     * Instantiates a new FXIObjectModifier.
     *
     * @param object the object being modified
     * @param stage  the stage
     * @throws IllegalAccessException    the illegal access exception
     * @throws InstantiationException    the instantiation exception
     * @throws InvocationTargetException the invocation target exception
     * @throws NoSuchMethodException     the no such method exception
     */
    public FXIObjectModifier(final T object, final Stage stage) throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        super(object.getClass(), stage);
        super.setCurrentObject(object);

        Set<Map.Entry<Field, Control>> entrySet = controls.entrySet();
        for(Map.Entry<Field, Control> entry : entrySet) {
            Control control = entry.getValue();
            if(control instanceof TextField) {
                ((TextField)control).setText(PropertyUtils.getProperty(object, entry.getKey().getName()).toString());
            } else if (control instanceof ComboBox) {
                ((ComboBox)control).setValue(PropertyUtils.getProperty(object, entry.getKey().getName()));
            } else if (control instanceof DatePicker) {
                ((DatePicker)control).setValue(((Date) PropertyUtils.getProperty(object, entry.getKey().getName())).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            } else if (control instanceof CheckBox) {
                ((CheckBox)control).setSelected((Boolean) PropertyUtils.getProperty(object, entry.getKey().getName()));
            }
        }
    }
}
