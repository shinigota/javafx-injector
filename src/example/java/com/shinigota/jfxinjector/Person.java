/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;

import java.util.Date;

public class Person {
    private int id;

    @FXMLField
    private String firstName;

    @FXMLField
    private String lastName;

    @FXMLField(nullable = true, description = "Optional field.")
    private String address;

    @FXMLField(description = "Mandatory integer.")
    private int numberOfComputers;

    @FXMLField
    private City city;

    @FXMLField
    private Date dateOfBirth;

    @FXMLField
    private boolean tallerThan6ft;


    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public int getNumberOfComputers() {
        return numberOfComputers;
    }

    public void setNumberOfComputers(final int numberOfComputers) {
        this.numberOfComputers = numberOfComputers;
    }

    public City getCity() {
        return city;
    }

    public void setCity(final City city) {
        this.city = city;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(final Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isTallerThan6ft() {
        return tallerThan6ft;
    }

    public void setTallerThan6ft(final boolean tallerThan6ft) {
        this.tallerThan6ft = tallerThan6ft;
    }
}
