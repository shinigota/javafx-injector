/*
 * Copyright (C) 2016  Benjamin Barbe
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.shinigota.jfxinjector;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class App extends Application{
    public static void main( String[] args ) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        final ArrayList<City> cities = new ArrayList<City>();

        City c = new City();
        c.setCityName("New York");
        c.setPostalCode(10007);
        cities.add(c);

        c = new City();
        c.setCityName("Manhattan");
        c.setPostalCode(54414);
        cities.add(c);

        FlowPane root = new FlowPane();
        Scene scene = new Scene(root, 300, 250);
        Button createPerson = new Button("Create a person");
        Button createCity = new Button("Create a city");
        Button displayCities = new Button("Display cities");
        root.getChildren().add(createPerson);
        root.getChildren().add(createCity);
        root.getChildren().add(displayCities);

        createPerson.setOnAction(event -> {
            FXIObjectCreator personCreator = FXIViewBuilder.buildCreator(Person.class, primaryStage);
            personCreator.bindData("city", cities);
            Person person = FXIViewBuilder.consumeCreator(personCreator);

            FXIObjectModifier personModifier = FXIViewBuilder.buildModifier(person, primaryStage);
            person = FXIViewBuilder.consumeCreator(personModifier);




            if(person != null)
                FXIViewBuilder.buildViewer(person, primaryStage);
        });



        createCity.setOnAction(event -> {
            FXIObjectCreator cityCreator = FXIViewBuilder.buildCreator(City.class, primaryStage);
            City city = FXIViewBuilder.consumeCreator(cityCreator);
            if(city != null) {
                cities.add(city);
                FXIViewBuilder.buildViewer(city, primaryStage);
            }
        });

        displayCities.setOnAction(event -> {
            FXITableListener deleteCallback = (FXITableListener) object -> {
                System.out.println("removed = " + cities.remove(object));
            };
            FXITableViewer tableViewer = FXIViewBuilder.buildTableViewer(cities, City.class, primaryStage, null, deleteCallback);
        });

        primaryStage.setTitle("Example");
        primaryStage.setScene(scene);
        primaryStage.show();


    }
}
